//
//  Test.swift
//  CodeChallengeTests
//
//  Created by Pedro Ribeiro on 29/09/2019.
//  Copyright © 2019 Pedro Ribeiro. All rights reserved.
//

import Foundation
@testable import CodeChallenge

struct Locations {
    
    static func getLocation() -> Location? {
        let locationData = JSONReader.read(fileName: "Location")
        do {
            let location = try JSONDecoder().decode(Location.self, from: locationData!)
            return location
        } catch {
            return nil
        }
    }
}
