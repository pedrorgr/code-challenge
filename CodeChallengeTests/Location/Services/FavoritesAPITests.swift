//
//  FavoritesAPITests.swift
//  CodeChallengeTests
//
//  Created by Pedro Ribeiro on 30/09/2019.
//  Copyright © 2019 Pedro Ribeiro. All rights reserved.
//

import XCTest
import RealmSwift
@testable import CodeChallenge

class FavoritesAPITests: XCTestCase {
    
    var sut: FavoritesAPI!
    var testFavorite1: Favorite!
    var testFavorite2: Favorite!
    var testLocation: Location!

    override func setUp() {
        super.setUp()
        setupFavoritesAPI()
    }
    
    override func tearDown() {
        resetFavoritesAPI()
        super.tearDown()
    }
    
    func setupFavoritesAPI() {
        sut = FavoritesAPI()
        
        Realm.Configuration.defaultConfiguration.inMemoryIdentifier = "TestDB"
        addFavoritesToDabase()
    }

    func resetFavoritesAPI() {
        sut = nil
        testFavorite1 = nil
        testFavorite2 = nil
        cleanDatabase()
    }
    
    // MARK: - Database
    func addFavoritesToDabase() {
        let realm = try! Realm()
        
        testFavorite1 = Favorite()
        testFavorite1.id = "12345"
        
        testFavorite2 = Favorite()
        testFavorite2.id = "54321"
        
        try! realm.write {
            realm.add(testFavorite1)
            realm.add(testFavorite2)
        }
    }
    
    func cleanDatabase() {
        let realm = try! Realm()

        // Clean up previous
        try! realm.write {
            realm.delete(realm.objects(Favorite.self))
        }
    }
    
    
    func test_FetchFavorite_IdNotNull_ReturnFavorite() {
        
        var fetchedFavorite: Favorite?
        let id = "12345"
        
        let expect = expectation(description: "Wait for fetchFavorite()")
        sut.fetchFavorite(id: id) { (favorite) in
            fetchedFavorite = favorite
            expect.fulfill()
        }
        waitForExpectations(timeout: 1.0)
        
        XCTAssertEqual(fetchedFavorite!.id, id)
    }
    
    func test_FetchFavorites_ReturnFavoritesNotEmpty() {
        
        var fetchedFavorites: [Favorite] = []
        
        let expect = expectation(description: "Wait for fetchFavorites()")
        sut.fetchFavorites(completion: { (favorites) in
            fetchedFavorites = favorites
            expect.fulfill()
        })
        waitForExpectations(timeout: 1.0)
        
        XCTAssertFalse(fetchedFavorites.isEmpty)
        XCTAssertEqual(fetchedFavorites[0].id, testFavorite1.id)
        XCTAssertEqual(fetchedFavorites[1].id, testFavorite2.id)
    }
    
    func test_SaveLocation_LocationNotNull_FavoriteLocation() {
        
        testLocation = Locations.getLocation()
        let expect = expectation(description: "Wait for saveLocation()")

        sut.saveLocation(location: testLocation!) { (saved) in
            expect.fulfill()
        }
        
        let realm = try! Realm()
         
        let savedFavorite = realm.objects(Favorite.self).last!
        
        waitForExpectations(timeout: 1.0)

        XCTAssertEqual(savedFavorite.id, testLocation.id)
    }
    
}
