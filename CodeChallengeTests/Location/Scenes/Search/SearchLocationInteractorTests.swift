//
//  SearchLocationInteractorTests.swift
//  CodeChallengeTests
//
//  Created by Pedro Ribeiro on 28/09/2019.
//  Copyright © 2019 Pedro Ribeiro. All rights reserved.
//

import XCTest
@testable import CodeChallenge

class SearchLocationInteractorTests: XCTestCase {
    
    class SearchLocationPresentationLogicSpy: SearchLocationPresentationLogic {
        
        var presentSearchResultsCalled = false
        var searchResponse: SearchLocation.FetchSearchResults.Response!
        
        func presentSearchResults(response: SearchLocation.FetchSearchResults.Response) {
            presentSearchResultsCalled = true
            searchResponse = response
        }
    }
    
    class LocationWorkerSpy: LocationWorker {
        
        var performSearchCalled = false
        var results: [Item] = []
        var error: LocationStoreError?
        
        override func performSearch(query: String, latitude: String, longitude: String, completion: @escaping ([Item], LocationStoreError?) -> Void) {
            completion(results, error)
        }
    }
    
    var sut: SearchLocationInteractor!
    var location1: Item!
    var location2: Item!
    
    override func setUp() {
        
        setupSearchLocationInteractor()
        setupItemsToTest()
    }
    
    override func tearDown() {
        
        resetSearchLocationInteractor()
        super.tearDown()
    }
    
    func setupSearchLocationInteractor() {
        sut = SearchLocationInteractor()
        
        ServiceFactory.shared.configure(type: .mock)
    }
    
    func setupItemsToTest() {
        location1 = Item(id: "", title: "", distance: 20, position: [], href: "")
        location2 = Item(id: "", title: "", distance: 5, position: [], href: "")
    }
    
    func resetSearchLocationInteractor() {
        sut = nil
        location1 = nil
        location2 = nil
    }

    func test_PerformSearch_RetriveSortedResults() {
        
        let searchLocationPresentationLogicSpy = SearchLocationPresentationLogicSpy()
        sut.presenter = searchLocationPresentationLogicSpy
        let locationWorkerSpy = LocationWorkerSpy(locationStore: ServiceFactory.shared.locationService())
        sut.locationWorker = locationWorkerSpy
        sut.latitude = "1213"
        sut.longitude = "123"
        locationWorkerSpy.results = [location1, location2]
        
        let request = SearchLocation.FetchSearchResults.Request(query: "test")
        sut.performSearch(request: request)
        
        let firstResult = searchLocationPresentationLogicSpy.searchResponse.results.first
        let secoundResult = searchLocationPresentationLogicSpy.searchResponse.results.last

        XCTAssert(searchLocationPresentationLogicSpy.presentSearchResultsCalled)
        XCTAssert(searchLocationPresentationLogicSpy.searchResponse.results.count > 0)
        XCTAssert(firstResult == location2)
        XCTAssert(secoundResult == location1)
    }
    
}
