//
//  SearchLocationViewController.swift
//  CodeChallengeTests
//
//  Created by Pedro Ribeiro on 30/09/2019.
//  Copyright © 2019 Pedro Ribeiro. All rights reserved.
//

import Foundation

import XCTest
@testable import CodeChallenge

class SearchLocationViewControllerTests: XCTestCase {
    
    class SearchLocationBusinessLogicSpy: SearchLocationBusinessLogic {
        
        var performSearchCalled = false
        var fetchLocationCalled = false
        
        var query = ""
        
        func fetchLocation(request: SearchLocation.FetchLocation.Request) {
            fetchLocationCalled = true
        }
        
        func performSearch(request: SearchLocation.FetchSearchResults.Request) {
            query = request.query
            performSearchCalled = true
        }
    }
    
    class SearchLocationRouterSpy: SearchLocationRouter {
        
        var routeToDetailsPageCalled = false
        
        override func routeToDetailsPage() {
            routeToDetailsPageCalled = true
        }
    }
    
    var sut: SearchLocationViewController!
    var window: UIWindow!
    
    override func setUp() {
        setupWindow()
        setupSearchLocationViewController()
    }
    
    override func tearDown() {
        resetWindow()
        resetLocatioDetailsViewController()
        super.tearDown()
    }
    
    func setupWindow() {
        window = UIWindow()
    }
    
    func setupSearchLocationViewController() {
        sut = SearchLocationViewController()
    }
    
    func resetWindow() {
        window = nil
    }
    
    func resetLocatioDetailsViewController() {
        sut = nil
    }
    
    func loadView() {
        window.addSubview(sut.view)
        RunLoop.current.run(until: Date())
    }
    
    func test_searchBarTextDidEndEditing_() {
        let searchLocationBusinessLogicSpy = SearchLocationBusinessLogicSpy()
        sut.interactor = searchLocationBusinessLogicSpy
        let tableViewSpy = UITableView()
        sut.tableView = tableViewSpy
    
        sut.searchLocations()
        
        XCTAssert(searchLocationBusinessLogicSpy.performSearchCalled)
    }
    
    func test_LoadView_FetchLocationCalled() {
        let searchLocationBusinessLogicSpy = SearchLocationBusinessLogicSpy()
        sut.interactor = searchLocationBusinessLogicSpy

        loadView()

        XCTAssert(searchLocationBusinessLogicSpy.fetchLocationCalled)
    }
    
    func test_DidSelectRow_RouteToLocationDetails() {
        let searchLocationRouterSpy = SearchLocationRouterSpy()
        sut.router = searchLocationRouterSpy
        let tableViewSpy = UITableView()
        sut.tableView = tableViewSpy
        let searchBarSpy = UISearchBar()
        sut.searchBar = searchBarSpy
        
        let indexPath = IndexPath(row: 0, section: 0)
        sut.tableView(tableViewSpy, didSelectRowAt: indexPath)
        
        XCTAssert(searchLocationRouterSpy.routeToDetailsPageCalled)
        XCTAssertFalse(searchBarSpy.isFirstResponder)
        XCTAssertEqual(sut.selectedIndexPath, indexPath)
    }
}

