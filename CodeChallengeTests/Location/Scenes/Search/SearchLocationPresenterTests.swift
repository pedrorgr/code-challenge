//
//  SearchLocationPresenterTests.swift
//  CodeChallengeTests
//
//  Created by Pedro Ribeiro on 30/09/2019.
//  Copyright © 2019 Pedro Ribeiro. All rights reserved.
//

import XCTest
@testable import CodeChallenge

class SearchLocationPresenterTests: XCTestCase {
    
    class SearchLocationDisplayLogicSpy: SearchLocationDisplayLogic {
        
        var displaySearchResultsCalled = false
        
        var displayedResults: [SearchLocation.FetchSearchResults.ViewModel.DisplayedResults] = []
        var errorTitle: String?
        var errorMessage: String?
        func displaySearchResults(viewModel: SearchLocation.FetchSearchResults.ViewModel) {
            displayedResults = viewModel.displayedResults
            errorTitle = viewModel.errorTitle
            errorMessage = viewModel.errorMessage
            displaySearchResultsCalled = true
        }
    }
    
    var sut: SearchLocationPresenter!
    
    override func setUp() {
        setupSearchLocationPresenter()
    }
    
    override func tearDown() {
        resetLocatioDetailsPresenter()
        super.tearDown()
    }
    
    func setupSearchLocationPresenter() {
        sut = SearchLocationPresenter()
    }
    
    func resetLocatioDetailsPresenter() {
        sut = nil
    }
    
    func test_PresentDetails_EmptySearchResults_RetriveEmptyDisplayedResults() {
        let searchLocationDisplayLogic = SearchLocationDisplayLogicSpy()
        sut.viewController = searchLocationDisplayLogic
        
        let results:[Item] = []
        let response = SearchLocation.FetchSearchResults.Response(results: results, error: nil)
        sut.presentSearchResults(response: response)
        
        XCTAssert(searchLocationDisplayLogic.displaySearchResultsCalled)
        XCTAssertNil(searchLocationDisplayLogic.errorTitle)
        XCTAssertNil(searchLocationDisplayLogic.errorMessage)
        XCTAssertTrue(searchLocationDisplayLogic.displayedResults.isEmpty)
    }
    
    func test_PresentDetails_ErrorSearchingResults_RetriveErrorTitleAndErrorMessage() {
        let searchLocationDisplayLogic = SearchLocationDisplayLogicSpy()
        sut.viewController = searchLocationDisplayLogic
        
        let results:[Item] = []
        let errorMessage = "error message"
        let response = SearchLocation.FetchSearchResults.Response(results: results, error: LocationStoreError.LocationRequestError(errorMessage))
        sut.presentSearchResults(response: response)
        
        XCTAssert(searchLocationDisplayLogic.displaySearchResultsCalled)
        XCTAssertNotNil(searchLocationDisplayLogic.errorTitle)
        XCTAssertNotNil(searchLocationDisplayLogic.errorMessage)
        XCTAssertEqual(searchLocationDisplayLogic.errorMessage, errorMessage)
        XCTAssertTrue(searchLocationDisplayLogic.displayedResults.isEmpty)
    }
}

