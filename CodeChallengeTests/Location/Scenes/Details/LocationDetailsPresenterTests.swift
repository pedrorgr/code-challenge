//
//  LocationDetailsPresenterTests.swift
//  CodeChallengeTests
//
//  Created by Pedro Ribeiro on 30/09/2019.
//  Copyright © 2019 Pedro Ribeiro. All rights reserved.
//

import XCTest
@testable import CodeChallenge

class LocationDetailsPresenterTests: XCTestCase {
    
    class LocationDetailsDisplayLogicSpy: LocationDetailsDisplayLogic {
        
        var displayDetailsCalled = false
        var displaySavedFavoriteCalled = false
        
        var viewModelToDisplay: LocationDetails.FetchDetails.ViewModel!
        
        func displayDetails(viewModel: LocationDetails.FetchDetails.ViewModel) {
            viewModelToDisplay = viewModel
            displayDetailsCalled = true
        }
        
        func displaySavedFavorite(viewModel: LocationDetails.SaveToFavorites.ViewModel) {
            displaySavedFavoriteCalled = true
        }
    }
    
    var sut: LocationDetailsPresenter!
    var locationToTest: Location!
    
    override func setUp() {
        
        setupLocationDetailsPresenter()
    }
    
    override func tearDown() {
        
        resetLocatioDetailsPresenter()
        resetLocationToTest()
        super.tearDown()
    }
    
    func setupLocationDetailsPresenter() {
        sut = LocationDetailsPresenter()
    }
    
    func resetLocatioDetailsPresenter() {
        sut = nil
    }
    
    func resetLocationToTest() {
        locationToTest = nil
    }
    
    func test_PresentDetails_ValidLocation_RetriveDisplayedLocation() {
        let locationDetailsDisplayLogic = LocationDetailsDisplayLogicSpy()
        sut.viewController = locationDetailsDisplayLogic
        
        locationToTest = Locations.getLocation()
        let response = LocationDetails.FetchDetails.Response(location: locationToTest!, error: nil, distance: 20)
        sut.presentDetails(response: response)
    
        XCTAssertNotNil(locationDetailsDisplayLogic.viewModelToDisplay.displayedLocation)
        XCTAssert(locationDetailsDisplayLogic.displayDetailsCalled)
    }
    
    func test_PresentSavedFavorite_DisplaySavedFavoriteCalled() {
        let locationDetailsDisplayLogic = LocationDetailsDisplayLogicSpy()
        sut.viewController = locationDetailsDisplayLogic
        
        let response = LocationDetails.SaveToFavorites.Response(isSaved: true)
        sut.presentSavedFavorite(response: response)
        
        XCTAssert(locationDetailsDisplayLogic.displaySavedFavoriteCalled)
    }
}
