//
//  LocationDetailsInteractorTests.swift
//  CodeChallengeTests
//
//  Created by Pedro Ribeiro on 29/09/2019.
//  Copyright © 2019 Pedro Ribeiro. All rights reserved.
//

import XCTest
@testable import CodeChallenge

class LocationDetailsInteractorTests: XCTestCase {
    
    class LocationDetailsPresentationLogicSpy: LocationDetailsPresentationLogic {

        var presentDetailsCalled = false
        var presentSavedFavoriteCalled = false
        
        var savedFavoriteResponse: Bool = false
        
        func presentDetails(response: LocationDetails.FetchDetails.Response) {
            presentDetailsCalled = true
        }
        
        func presentSavedFavorite(response: LocationDetails.SaveToFavorites.Response) {
            presentSavedFavoriteCalled = true
            savedFavoriteResponse = response.isSaved
        }
    }
    
    class LocationWorkerSpy: LocationWorker {
        
        var performSearchCalled = false
        var location: Location?
        var error: LocationStoreError?
        
        override func fetchLocationDetails(url: String, completion: @escaping (Location?, LocationStoreError?) -> Void) {
            performSearchCalled = true
            completion(location, error)
        }
    }
    
    class FavoritesWorkerSpy: FavoritesWorker {
        
        var fetchFavoriteCalled = false
        var saveLocationCalled = false
        var removeLocationCalled = false
        
        var favorite: Favorite?
        
        override func fetchFavorite(id: String, completion: @escaping (Favorite?) -> Void) {
            fetchFavoriteCalled = true
            completion(favorite)
        }
 
        override func saveLocation(location: Location, completion: @escaping (Bool) -> Void) {
            saveLocationCalled = true
            completion(true)
        }
        
        override func removeLocation(locationId: String, completion: @escaping (Bool) -> Void) {
            removeLocationCalled = true
            completion(true)
        }
    }
    
    var sut: LocationDetailsInteractor!
    var location: Location!
    
    override func setUp() {
        
        setupSearchLocationInteractor()
    }
    
    override func tearDown() {
        
        resetSearchLocationInteractor()
        super.tearDown()
    }
    
    func setupSearchLocationInteractor() {
        sut = LocationDetailsInteractor()
        
        ServiceFactory.shared.configure(type: .mock)
    }

    func resetSearchLocationInteractor() {
        sut = nil
    }

    func test_FetchDetails_RetriveLocation() {
        
        let locationDetailsPresentationLogicSpy = LocationDetailsPresentationLogicSpy()
        sut.presenter = locationDetailsPresentationLogicSpy
        let locationWorkerSpy = LocationWorkerSpy(locationStore: ServiceFactory.shared.locationService())
        sut.worker = locationWorkerSpy
        
        let request = LocationDetails.FetchDetails.Request()
        sut.fetchDetails(request: request)

        XCTAssert(locationDetailsPresentationLogicSpy.presentDetailsCalled)
    }
    
    func test_FetchFavorite_FetchFavoriteCalled() {
        
        let locationDetailsPresentationLogicSpy = LocationDetailsPresentationLogicSpy()
        sut.presenter = locationDetailsPresentationLogicSpy
        let favoritesWorkerSpy = FavoritesWorkerSpy(favoritesStore:ServiceFactory.shared.favoritesService())
        sut.favoritesWorker = favoritesWorkerSpy
        favoritesWorkerSpy.favorite = Favorite()
        
        let request = LocationDetails.SaveToFavorites.Request(save: true)
        sut.fetchFavorite(request: request)

        XCTAssert(locationDetailsPresentationLogicSpy.presentSavedFavoriteCalled)
        XCTAssert(locationDetailsPresentationLogicSpy.savedFavoriteResponse)
    }
    
    func test_FetchFavorites_ToSave_SaveToFavoritesCalled() {
        
        let locationDetailsPresentationLogicSpy = LocationDetailsPresentationLogicSpy()
        sut.presenter = locationDetailsPresentationLogicSpy
        let favoritesWorkerSpy = FavoritesWorkerSpy(favoritesStore:ServiceFactory.shared.favoritesService())
        sut.favoritesWorker = favoritesWorkerSpy
        sut.location = Locations.getLocation()
        
        let request = LocationDetails.SaveToFavorites.Request(save: true)
        sut.updateToFavorites(request: request)
        
        XCTAssert(favoritesWorkerSpy.saveLocationCalled)
        XCTAssert(locationDetailsPresentationLogicSpy.presentSavedFavoriteCalled)
    }
    
    func test_UpdateToFavorites_ToNotSave_RemoveCalled() {
        
        let locationDetailsPresentationLogicSpy = LocationDetailsPresentationLogicSpy()
        sut.presenter = locationDetailsPresentationLogicSpy
        let favoritesWorkerSpy = FavoritesWorkerSpy(favoritesStore:ServiceFactory.shared.favoritesService())
        sut.favoritesWorker = favoritesWorkerSpy
        sut.location = Locations.getLocation()
        
        let request = LocationDetails.SaveToFavorites.Request(save: false)
        sut.updateToFavorites(request: request)
        
        XCTAssert(favoritesWorkerSpy.removeLocationCalled)
        XCTAssert(locationDetailsPresentationLogicSpy.presentSavedFavoriteCalled)
    }
    
}
