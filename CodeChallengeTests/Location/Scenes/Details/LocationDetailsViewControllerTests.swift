//
//  LocationDetailsViewControllerTests.swift
//  CodeChallengeTests
//
//  Created by Pedro Ribeiro on 30/09/2019.
//  Copyright © 2019 Pedro Ribeiro. All rights reserved.
//

import Foundation

import XCTest
@testable import CodeChallenge

class LocationDetailsViewControllerTests: XCTestCase {
    
    class LocationDetailsBusinessLogicSpy: LocationDetailsBusinessLogic {
        
        var fetchDetailsCalled = false
        var fetchFavoriteCalled = false
        var updateToFavoritesCalled = false
        
        var save = false
        
        func fetchDetails(request: LocationDetails.FetchDetails.Request) {
            fetchDetailsCalled = true
        }
        
        func fetchFavorite(request: LocationDetails.SaveToFavorites.Request) {
            fetchFavoriteCalled = true
        }
        
        func updateToFavorites(request: LocationDetails.SaveToFavorites.Request) {
            save = request.save
            updateToFavoritesCalled = true
        }
    }
    
    var sut: LocationDetailsViewController!
    var window: UIWindow!
    
    override func setUp() {
        setupWindow()
        setupLocationDetailsViewController()
    }
    
    override func tearDown() {
        resetWindow()
        resetLocatioDetailsViewController()
        super.tearDown()
    }
    
    func setupWindow() {
        window = UIWindow()
    }
    
    func setupLocationDetailsViewController() {
        sut = LocationDetailsViewController()
    }
    
    func resetWindow() {
        window = nil
    }
    
    func resetLocatioDetailsViewController() {
        sut = nil
    }
    
    func loadView() {
        window.addSubview(sut.view)
        RunLoop.current.run(until: Date())
    }
    
    func test_LoadView_FetchDetailsCalled() {
        
        let locationDetailsBusinessLogicSpy = LocationDetailsBusinessLogicSpy()
        sut.interactor = locationDetailsBusinessLogicSpy
        
        loadView()
        
        XCTAssert(locationDetailsBusinessLogicSpy.fetchDetailsCalled)
    }
    
    func test_LoadView_FetchFavoriteCalled() {
        
        let locationDetailsBusinessLogicSpy = LocationDetailsBusinessLogicSpy()
        sut.interactor = locationDetailsBusinessLogicSpy
        
        loadView()
        
        XCTAssert(locationDetailsBusinessLogicSpy.fetchFavoriteCalled)
    }
    
    func test_FavoriteButtonTapped_FavoriteNotSelected_RequestToSave() {
        
        let locationDetailsBusinessLogicSpy = LocationDetailsBusinessLogicSpy()
        sut.interactor = locationDetailsBusinessLogicSpy
        
        let buttonSpy = UIButton()
        buttonSpy.isSelected = false
        sut.favoriteButtonTapped(buttonSpy)
        
        XCTAssert(locationDetailsBusinessLogicSpy.updateToFavoritesCalled)
        XCTAssert(locationDetailsBusinessLogicSpy.save)
    }
}
