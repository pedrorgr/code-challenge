//
//  LocationDetailsViewController.swift
//  CodeChallenge
//
//  Created by Pedro Ribeiro on 28/09/2019.
//  Copyright (c) 2019 Pedro Ribeiro. All rights reserved.
//

import UIKit
import MapKit

protocol LocationDetailsDisplayLogic: class {
    func displayDetails(viewModel: LocationDetails.FetchDetails.ViewModel)
    func displaySavedFavorite(viewModel: LocationDetails.SaveToFavorites.ViewModel)
}

class LocationDetailsViewController: BaseViewController {
    var interactor: LocationDetailsBusinessLogic?
    var router: (NSObjectProtocol & LocationDetailsRoutingLogic & LocationDetailsDataPassing)?
        
    @IBOutlet weak var detailsStackView: UIStackView!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var postalCode: UILabel!
    @IBOutlet weak var distance: UILabel!
    @IBOutlet weak var streetName: UILabel!
    @IBOutlet weak var coordinates: UILabel!
    
    @IBOutlet weak var favoriteButton: UIButton!
    var loadingView: LoadingViewController!
        
    convenience init() {
        self.init(nibName: "LocationDetailsViewController")
    }
    
    override func setup() {
        LocationDetailsConfigurator.shared.configure(self)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        detailsStackView.alpha = 0.0
        fetchDetails()
        fetchFavorite()
    }

    @IBAction func favoriteButtonTapped(_ sender: UIButton) {
        let request = LocationDetails.SaveToFavorites.Request(save: !sender.isSelected)
        interactor?.updateToFavorites(request: request)
    }

    func fetchFavorite() {
        let request = LocationDetails.SaveToFavorites.Request(save: false)
        interactor?.fetchFavorite(request: request)
    }
    
    func fetchDetails() {
        
        loadingView = LoadingViewController()
        add(loadingView)
        
        let request = LocationDetails.FetchDetails.Request()
        interactor?.fetchDetails(request: request)
    }
}

extension LocationDetailsViewController: LocationDetailsDisplayLogic {
    
    func displayDetails(viewModel: LocationDetails.FetchDetails.ViewModel) {

        loadingView.remove()

        if let errorMessage = viewModel.errorMessage, let errorTitle = viewModel.errorTitle {
            alert(title: errorTitle, message: errorMessage)
            detailsStackView.alpha = 0.0
        } else {
            streetName.text = viewModel.displayedLocation!.street
            postalCode.text = viewModel.displayedLocation!.postalCode
            distance.text = viewModel.displayedLocation!.distance
            setupPosition(position: viewModel.displayedLocation!.coordinates)
            detailsStackView.alpha = 1.0
        }
    }
    
    func setupPosition(position: [Double]) {
        
        guard let latitude = position.first, let longitude = position.last else {
            return
        }
        let annotation = MKPointAnnotation()
        let locationCoordinate =  CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        annotation.coordinate = locationCoordinate
        mapView.centerCoordinate = locationCoordinate
        mapView.addAnnotation(annotation)
        coordinates.text = "Coordinates: \(latitude.toString()), \(longitude.toString())"
    }
    
    func displaySavedFavorite(viewModel: LocationDetails.SaveToFavorites.ViewModel) {
        favoriteButton.setTitle(viewModel.favoriteButtonText, for: .normal)
        favoriteButton.isSelected = viewModel.isSaved
    }
}
