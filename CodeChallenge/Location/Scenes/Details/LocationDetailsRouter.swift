//
//  LocationDetailsRouter.swift
//  CodeChallenge
//
//  Created by Pedro Ribeiro on 29/09/2019.
//  Copyright © 2019 Pedro Ribeiro. All rights reserved.
//

import UIKit

@objc protocol LocationDetailsRoutingLogic {
}

protocol LocationDetailsDataPassing {
    var dataStore: LocationDetailsDataStore? { get }
}

class LocationDetailsRouter: NSObject, LocationDetailsRoutingLogic, LocationDetailsDataPassing {
    weak var viewController: LocationDetailsViewController?
    var dataStore: LocationDetailsDataStore?
}
