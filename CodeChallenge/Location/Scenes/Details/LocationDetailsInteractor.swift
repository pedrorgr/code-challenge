//
//  LocationDetailsInteractor.swift
//  CodeChallenge
//
//  Created by Pedro Ribeiro on 28/09/2019.
//  Copyright (c) 2019 Pedro Ribeiro. All rights reserved.
//

import UIKit

protocol LocationDetailsBusinessLogic {
    func fetchDetails(request: LocationDetails.FetchDetails.Request)
    func fetchFavorite(request: LocationDetails.SaveToFavorites.Request)
    func updateToFavorites(request: LocationDetails.SaveToFavorites.Request)
}

protocol LocationDetailsDataStore {
    var url: String { get set }
    var distance: Int { get set }
    var id: String { get set }
}

class LocationDetailsInteractor: LocationDetailsBusinessLogic, LocationDetailsDataStore {
    var presenter: LocationDetailsPresentationLogic?
    var worker = LocationWorker(locationStore: ServiceFactory.shared.locationService())
    var favoritesWorker = FavoritesWorker(favoritesStore: ServiceFactory.shared.favoritesService())
    
    var url: String = ""
    var distance: Int = 0
    var location: Location?
    var id: String = ""
        
    func fetchDetails(request: LocationDetails.FetchDetails.Request) {
        
        worker.fetchLocationDetails(url: url) { [weak self] (location, error) in
            self?.location = location
            let response = LocationDetails.FetchDetails.Response(location: location, error: error, distance: self?.distance)
            self?.presenter?.presentDetails(response: response)
        }
    }

    func fetchFavorite(request: LocationDetails.SaveToFavorites.Request) {
        favoritesWorker.fetchFavorite(id: id) { [weak self] (favorite) in
            let response = LocationDetails.SaveToFavorites.Response(isSaved: favorite != nil)
            self?.presenter?.presentSavedFavorite(response: response)
        }
    }
    
    func updateToFavorites(request: LocationDetails.SaveToFavorites.Request) {
        guard let location = location else {
            return
        }
        
        if request.save {
            favoritesWorker.saveLocation(location: location) { [weak self] (saved) in
                let response = LocationDetails.SaveToFavorites.Response(isSaved: saved)
                self?.presenter?.presentSavedFavorite(response: response)
            }
        } else {
            favoritesWorker.removeLocation(locationId: location.id) { [weak self] (removed) in
                let response = LocationDetails.SaveToFavorites.Response(isSaved: !removed)
                self?.presenter?.presentSavedFavorite(response: response)
            }
        }
    }

}
