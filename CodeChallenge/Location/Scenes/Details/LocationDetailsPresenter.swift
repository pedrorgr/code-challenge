//
//  LocationDetailsPresenter.swift
//  CodeChallenge
//
//  Created by Pedro Ribeiro on 28/09/2019.
//  Copyright (c) 2019 Pedro Ribeiro. All rights reserved.

import UIKit

protocol LocationDetailsPresentationLogic {
    func presentDetails(response: LocationDetails.FetchDetails.Response)
    func presentSavedFavorite(response: LocationDetails.SaveToFavorites.Response)
}

class LocationDetailsPresenter: LocationDetailsPresentationLogic {
    weak var viewController: LocationDetailsDisplayLogic?
    
    func presentDetails(response: LocationDetails.FetchDetails.Response) {
        
        let errorMessage: String?
        
        if let error = response.error {
            switch error {
            case .LocationParseError(let message):
                errorMessage = message
            case .LocationRequestError(let message):
                errorMessage = message
            }
        } else {
            errorMessage = nil
        }
                    
        if let location = response.location {
            let displayedLocation = LocationDetails.FetchDetails.ViewModel.DisplayedLocation(street: "Street Name: " + "\(location.streetName ?? "")",
                                                                                             postalCode: "Postal Code: " + "\(location.postalCode ?? "")",
                                                                                             coordinates: location.position,
                                                                                             distance: "Distance: \(response.distance ?? 0)")
            
            let viewModel = LocationDetails.FetchDetails.ViewModel(displayedLocation: displayedLocation, errorTitle: "Error", errorMessage: errorMessage)
             viewController?.displayDetails(viewModel: viewModel)
        } else {
            let viewModel = LocationDetails.FetchDetails.ViewModel(displayedLocation: nil, errorTitle: "Error", errorMessage: errorMessage)
             viewController?.displayDetails(viewModel: viewModel)
        }
    }
    
    func presentSavedFavorite(response: LocationDetails.SaveToFavorites.Response) {
        let text = response.isSaved ? "Remove from favorites" : "Save to favorites"
        let viewModel = LocationDetails.SaveToFavorites.ViewModel(isSaved: response.isSaved, favoriteButtonText: text)
        viewController?.displaySavedFavorite(viewModel: viewModel)
    }
}
