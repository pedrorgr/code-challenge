//
//  LocationDetailsModels.swift
//  CodeChallenge
//
//  Created by Pedro Ribeiro on 28/09/2019.
//  Copyright (c) 2019 Pedro Ribeiro. All rights reserved.
//

import UIKit

enum LocationDetails {
    
    enum FetchDetails {
        struct Request {
        }
        
        struct Response {
            var location: Location?
            var error: LocationStoreError?
            var distance: Int?
        }
        
        struct ViewModel {
            
            struct DisplayedLocation {
                var street: String
                var postalCode: String
                var coordinates: [Double]
                var distance: String
            }
            
            var displayedLocation: DisplayedLocation?
            var errorTitle: String?
            var errorMessage: String?
        }
    }
    
    enum SaveToFavorites {
        struct Request {
            var save: Bool
        }
        
        struct Response {
            var isSaved: Bool
        }
        
        struct ViewModel {
            var isSaved: Bool
            var favoriteButtonText: String
        }
    }
}
