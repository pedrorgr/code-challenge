//
//  LocationDetailsConfigurator.swift
//  CodeChallenge
//
//  Created by Pedro Ribeiro on 01/10/2019.
//  Copyright © 2019 Pedro Ribeiro. All rights reserved.
//

import Foundation

class LocationDetailsConfigurator {
    
    static let shared = LocationDetailsConfigurator()
    
    func configure(_ viewController: LocationDetailsViewController) {
        let interactor = LocationDetailsInteractor()
        let presenter = LocationDetailsPresenter()
        let router = LocationDetailsRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }
}
