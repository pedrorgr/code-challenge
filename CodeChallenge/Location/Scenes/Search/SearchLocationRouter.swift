//
//  SearchLocationRouter.swift
//  CodeChallenge
//
//  Created by Pedro Ribeiro on 26/09/2019.
//  Copyright (c) 2019 Pedro Ribeiro. All rights reserved.
//

import UIKit

@objc protocol SearchLocationRoutingLogic {
    func routeToDetailsPage()
    func routeToFavorites()
}

protocol SearchLocationDataPassing {
    var dataStore: SearchLocationDataStore? { get }
}

class SearchLocationRouter: NSObject, SearchLocationRoutingLogic, SearchLocationDataPassing {
    weak var viewController: SearchLocationViewController?
    var dataStore: SearchLocationDataStore?
    
    func routeToDetailsPage() {
        
        let destinationVC = LocationDetailsViewController()
        var destinationDS = destinationVC.router!.dataStore!
        passDataToDetails(source: dataStore!, destination: &destinationDS)
        navigateToDetails(source: viewController!, destination: destinationVC)
    }
    
    func navigateToDetails(source: SearchLocationViewController, destination: LocationDetailsViewController) {
        source.navigationController?.pushViewController(destination, animated: true)
    }
    
    func passDataToDetails(source: SearchLocationDataStore, destination: inout LocationDetailsDataStore) {
        if let indexPath = viewController!.selectedIndexPath {
            let item = source.results[indexPath.row]
            destination.url = item.href
            destination.distance = item.distance
            destination.id = item.id
        }
    }
    
    func routeToFavorites() {
        viewController!.present(FavoritesListViewController(), animated: true, completion: nil)
    }
}
