//
//  SearchLocationViewController.swift
//  CodeChallenge
//
//  Created by Pedro Ribeiro on 26/09/2019.
//  Copyright (c) 2019 Pedro Ribeiro. All rights reserved.
//


import UIKit

protocol SearchLocationDisplayLogic: class {
    func displaySearchResults(viewModel: SearchLocation.FetchSearchResults.ViewModel)
}

class SearchLocationViewController: BaseViewController {
    var interactor: SearchLocationBusinessLogic?
    var router: (NSObjectProtocol & SearchLocationRoutingLogic & SearchLocationDataPassing)?
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    var timer: Timer?
    var searchResults: [SearchLocation.FetchSearchResults.ViewModel.DisplayedResults] = []
    var selectedIndexPath: IndexPath?
    
    var loadingView: LoadingViewController!
    var query = ""
    
    private var state: State?
    private var shownViewController: UIViewController?
    
    convenience init() {
        self.init(nibName: "SearchLocationViewController")
    }
    
    override func setup() {
        SearchLocationConfigurator.shared.configure(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTitle()
        setupGoToFavoritesButton()
        setupTableView()
        
        fetchLocation()
    }
        
    func setupTitle() {
        title = "Search Locations"
    }
    
    func setupGoToFavoritesButton() {
        
        let favoritesButton = UIButton.init(type: .custom)
        favoritesButton.setTitle("Favorites", for: .normal)
        favoritesButton.setTitleColor(.black, for: .normal)
        favoritesButton.addTarget(self, action: #selector(goToFavorites), for: .touchUpInside)

        let profileBarButtonItem = UIBarButtonItem(customView: favoritesButton)
        navigationItem.rightBarButtonItem = profileBarButtonItem
    }
    
    func setupTableView() {
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: NSStringFromClass(type(of:UITableViewCell())))
        tableView.alpha = 0.0
    }
    
    func fetchLocation() {
        let request = SearchLocation.FetchLocation.Request()
        interactor?.fetchLocation(request: request)
    }
    
    @IBAction func goToFavorites(_ sender: Any) {
        router?.routeToFavorites()
    }
}

// MARK: UISearchBarDelegate
extension SearchLocationViewController: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()

        guard let text = searchBar.text else {
            return
        }

        if text.count > 0 {
            timer?.invalidate()
            timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(searchLocations), userInfo: text, repeats: false)
        }
    }

    @objc func searchLocations() {
        var query: String = ""
        if let text = timer?.userInfo as! String? {
            query = text
        }
        self.query = query
        performSearch(query: query)
    }
    
    func performSearch(query: String) {
        
        transition(to: .loading)

        let request = SearchLocation.FetchSearchResults.Request(query: query)
        interactor?.performSearch(request: request)
    }
    
}

// MARK: SearchLocationDisplayLogic
extension SearchLocationViewController: SearchLocationDisplayLogic {
    
    func displaySearchResults(viewModel: SearchLocation.FetchSearchResults.ViewModel) {

        if let errorTitle = viewModel.errorTitle, let errorMessage = viewModel.errorMessage {
            transition(to: .failed(title: errorTitle, message: errorMessage))
        } else {
            searchResults = viewModel.displayedResults
            tableView.reloadData()
            if viewModel.displayedResults.count == 0  {
                transition(to: .empty)
            } else {
                transition(to: .render)
            }
        }
    }
}

// MARK: UISearchBarDelegate
extension SearchLocationViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchResults.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(type(of:UITableViewCell())), for: indexPath)
        
        let location = searchResults[indexPath.row]
        cell.textLabel?.text = location.title
        return cell
    }
    
}
extension SearchLocationViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndexPath = indexPath
        searchBar.resignFirstResponder()
        router?.routeToDetailsPage()
    }
}

extension SearchLocationViewController {
    
    enum State {
        case loading
        case failed(title: String, message: String)
        case render
        case empty
    }
    
    func transition(to newState: State) {
        
        shownViewController?.remove()

        switch newState {
        case .loading:
            showView(vc: LoadingViewController())
        case .failed(let error):
            alert(title: error.title, message: error.message, okActionHandler: {
                self.performSearch(query: self.query)
            })
        case .render:
            tableView.alpha = 1.0
        case .empty:
            tableView.alpha = 0.0
            showView(vc: EmptyViewController(text: "No results"))
        }
    
        state = newState
    }
    
    func showView(vc: UIViewController) {
        add(vc)
        shownViewController = vc
    }
}
