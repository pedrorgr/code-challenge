//
//  SearchLocationPresenter.swift
//  CodeChallenge
//
//  Created by Pedro Ribeiro on 26/09/2019.
//  Copyright (c) 2019 Pedro Ribeiro. All rights reserved.
//

import UIKit

protocol SearchLocationPresentationLogic {
    func presentSearchResults(response: SearchLocation.FetchSearchResults.Response)
}

class SearchLocationPresenter: SearchLocationPresentationLogic {
    weak var viewController: SearchLocationDisplayLogic?
    
    func presentSearchResults(response: SearchLocation.FetchSearchResults.Response) {
        
        let errorMessage: String?
        let errorTitle: String?
        
        if let error = response.error {
            errorTitle = "Ups error"
            switch error {
            case .LocationParseError(let message):
                errorMessage = message
            case .LocationRequestError(let message):
                errorMessage = message
            }
        } else {
            errorTitle = nil
            errorMessage = nil
        }
        
        var displayedResults: [SearchLocation.FetchSearchResults.ViewModel.DisplayedResults] = []
        
        for item in response.results {
            let displayedResult = SearchLocation.FetchSearchResults.ViewModel.DisplayedResults(title: item.title,
                                                                                               distance: item.distance)
            displayedResults.append(displayedResult)
        }
        
        let viewModel = SearchLocation.FetchSearchResults.ViewModel(displayedResults: displayedResults, errorMessage: errorMessage, errorTitle: errorTitle)
        viewController?.displaySearchResults(viewModel: viewModel)
    }

}
