//
//  SearchLocationModels.swift
//  CodeChallenge
//
//  Created by Pedro Ribeiro on 26/09/2019.
//  Copyright (c) 2019 Pedro Ribeiro. All rights reserved.
//

import UIKit

enum SearchLocation {
    
    enum FetchLocation {
        struct Request {
        }
        
        struct Response {
            var location: String
        }
        
        struct ViewModel {
            var location: String
        }
    }
    
    enum FetchSearchResults {
        struct Request {
            let query: String
        }
        
        struct Response {
            let results: [Item]
            let error: LocationStoreError?
        }
        
        struct ViewModel {
            struct DisplayedResults {
                let title: String
                let distance: Int
            }
            let displayedResults: [DisplayedResults]
            let errorMessage: String?
            let errorTitle: String?
        }
    }
}
