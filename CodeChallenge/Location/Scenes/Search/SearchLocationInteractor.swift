//
//  SearchLocationInteractor.swift
//  CodeChallenge
//
//  Created by Pedro Ribeiro on 26/09/2019.
//  Copyright (c) 2019 Pedro Ribeiro. All rights reserved.
//

import UIKit

protocol SearchLocationBusinessLogic {
    func fetchLocation(request: SearchLocation.FetchLocation.Request)
    func performSearch(request: SearchLocation.FetchSearchResults.Request)
}

protocol SearchLocationDataStore {
    var results: [Item] { get set}
}

class SearchLocationInteractor: SearchLocationBusinessLogic, SearchLocationDataStore {
    var presenter: SearchLocationPresentationLogic?
    var locationWorker = LocationWorker(locationStore: ServiceFactory.shared.locationService())
    var coreLocationWorker = CoreLocationWorker(coreLocationStore: ServiceFactory.shared.coreLocationService())
    
    var latitude: String?
    var longitude: String?
    var results: [Item] = []
    
    func fetchLocation(request: SearchLocation.FetchLocation.Request) {
        coreLocationWorker.fetchCurrentLocation { [weak self] (coordentes) in
            self?.latitude = "\(coordentes.first!)"
            self?.longitude = "\(coordentes.last!)"
        }
    }
    func performSearch(request: SearchLocation.FetchSearchResults.Request) {
        
        guard let lat = latitude, let long = longitude else {
            let response = SearchLocation.FetchSearchResults.Response(results: [], error: LocationStoreError.LocationRequestError("Your location couldn't be found"))
            presenter?.presentSearchResults(response: response)
            return
        }
        
        locationWorker.performSearch(query: request.query, latitude: lat, longitude: long) { [weak self] (results, error) in
            guard let weakSelf = self else {
                return
            }
            
            weakSelf.results = results.sorted(by: weakSelf.sorterForDistance)
            let response = SearchLocation.FetchSearchResults.Response(results: weakSelf.results, error: error)
            weakSelf.presenter?.presentSearchResults(response: response)
        }
        
    }
    
    private func sorterForDistance(this:Item, that:Item) -> Bool {
        return this.distance < that.distance
    }
}
