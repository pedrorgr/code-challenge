//
//  SearchLocationConfigurator.swift
//  CodeChallenge
//
//  Created by Pedro Ribeiro on 01/10/2019.
//  Copyright © 2019 Pedro Ribeiro. All rights reserved.
//

import Foundation

class SearchLocationConfigurator {
    
    static let shared = SearchLocationConfigurator()
    
    func configure(_ viewController: SearchLocationViewController) {
        let interactor = SearchLocationInteractor()
        let presenter = SearchLocationPresenter()
        let router = SearchLocationRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }
}
