//
//  FavoritesListViewController.swift
//  CodeChallenge
//
//  Created by Pedro Ribeiro on 01/10/2019.
//  Copyright (c) 2019 Pedro Ribeiro. All rights reserved.


import UIKit
import MapKit

protocol FavoritesListDisplayLogic: class {
    func displayFetchedFavorites(viewModel: FavoritesList.FetchFavorites.ViewModel)
}

class FavoritesListViewController: BaseViewController {
    var interactor: FavoritesListBusinessLogic?
    var router: (NSObjectProtocol & FavoritesListRoutingLogic & FavoritesListDataPassing)?
   
    struct ViewTraits {
        static let cornerRadious: CGFloat = 15.0
    }
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var closeButton: UIButton!
    
    var displayedFavorites: [FavoritesList.FetchFavorites.ViewModel.DisplayedFavorite] = []
    
    convenience init() {
        self.init(nibName: "FavoritesListViewController")
    }
    
    override func setup() {
        FavoritesListConfigurator.shared.configure(self)
    }
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupCloseButton()
        setupTableView()
        fetchFavorites()
    }
    
    func setupCloseButton() {
        closeButton.layer.cornerRadius = ViewTraits.cornerRadious
    }
    
    func setupTableView() {
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: NSStringFromClass(type(of:UITableViewCell())))
    }
    
    func fetchFavorites() {
        let request = FavoritesList.FetchFavorites.Request()
        interactor?.fetchFavorites(request: request)
    }
    @IBAction func closeButtonTapped(_ sender: Any) {
        router?.routeToSearchLocations()
    }
}

extension FavoritesListViewController: FavoritesListDisplayLogic {
    
    func displayFetchedFavorites(viewModel: FavoritesList.FetchFavorites.ViewModel) {
        
        displayedFavorites = viewModel.displayedFavorites
        tableView.reloadData()
    }
}

extension FavoritesListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return displayedFavorites.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(type(of:UITableViewCell())), for: indexPath)
        
        let favorite = displayedFavorites[indexPath.row]
        cell.textLabel?.text = favorite.name
        return cell
    }
}

extension FavoritesListViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let annotation = MKPointAnnotation()
        let favorite = displayedFavorites[indexPath.row]
        let locationCoordinate =  CLLocationCoordinate2D(latitude: favorite.coordinates.first!, longitude: favorite.coordinates.last!)
        annotation.coordinate = locationCoordinate
        mapView.centerCoordinate = locationCoordinate
        mapView.addAnnotation(annotation)
    }
}
