//
//  FavoritesListInteractor.swift
//  CodeChallenge
//
//  Created by Pedro Ribeiro on 01/10/2019.
//  Copyright (c) 2019 Pedro Ribeiro. All rights reserved.
//

import UIKit

protocol FavoritesListBusinessLogic {
    func fetchFavorites(request: FavoritesList.FetchFavorites.Request)
}

protocol FavoritesListDataStore {
}

class FavoritesListInteractor: FavoritesListBusinessLogic, FavoritesListDataStore {
    var presenter: FavoritesListPresentationLogic?
    let worker = FavoritesWorker(favoritesStore: ServiceFactory.shared.favoritesService())
    
    func fetchFavorites(request: FavoritesList.FetchFavorites.Request) {
        
        worker.fetchFavorites { [weak self] (favorites) in
            let response = FavoritesList.FetchFavorites.Response(favorites: favorites)
            self?.presenter?.presentFetchedFavorites(response: response)
        }
    }
}
