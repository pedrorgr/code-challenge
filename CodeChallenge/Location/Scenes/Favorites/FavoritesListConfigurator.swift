//
//  FavoritesListConfigurator.swift
//  CodeChallenge
//
//  Created by Pedro Ribeiro on 01/10/2019.
//  Copyright © 2019 Pedro Ribeiro. All rights reserved.
//

import Foundation

class FavoritesListConfigurator {
    
    static let shared = FavoritesListConfigurator()
    
    func configure(_ viewController: FavoritesListViewController) {
        let interactor = FavoritesListInteractor()
        let presenter = FavoritesListPresenter()
        let router = FavoritesListRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }
}
