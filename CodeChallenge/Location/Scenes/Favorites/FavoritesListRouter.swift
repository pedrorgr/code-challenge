//
//  FavoritesListRouter.swift
//  CodeChallenge
//
//  Created by Pedro Ribeiro on 01/10/2019.
//  Copyright (c) 2019 Pedro Ribeiro. All rights reserved.

import UIKit

@objc protocol FavoritesListRoutingLogic {
    func routeToSearchLocations()
}

protocol FavoritesListDataPassing {
    var dataStore: FavoritesListDataStore? { get }
}

class FavoritesListRouter: NSObject, FavoritesListRoutingLogic, FavoritesListDataPassing {
    weak var viewController: FavoritesListViewController?
    var dataStore: FavoritesListDataStore?
    
    func routeToSearchLocations() {
        viewController?.dismiss(animated: true, completion: nil)
    }
}
