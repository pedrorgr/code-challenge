//
//  FavoritesListPresenter.swift
//  CodeChallenge
//
//  Created by Pedro Ribeiro on 01/10/2019.
//  Copyright (c) 2019 Pedro Ribeiro. All rights reserved.
//

import UIKit

protocol FavoritesListPresentationLogic {
    func presentFetchedFavorites(response: FavoritesList.FetchFavorites.Response)
}

class FavoritesListPresenter: FavoritesListPresentationLogic {
    weak var viewController: FavoritesListDisplayLogic?
    
    func presentFetchedFavorites(response: FavoritesList.FetchFavorites.Response) {
        
        var displayedFavorites: [FavoritesList.FetchFavorites.ViewModel.DisplayedFavorite] = []
        for favorite in response.favorites {
            
            let displayedFavorite = FavoritesList.FetchFavorites.ViewModel.DisplayedFavorite(name: favorite.name ?? "Name Place Holder",
                                                                                             coordinates: getCoordinates(latitude: favorite.latitude, longitude: favorite.longitude))
            displayedFavorites.append(displayedFavorite)
        }
        
        let viewModel = FavoritesList.FetchFavorites.ViewModel(displayedFavorites: displayedFavorites)
        viewController?.displayFetchedFavorites(viewModel: viewModel)
    }
    
    private func getCoordinates(latitude: String?, longitude: String?) -> [Double] {
        
        guard let lat = latitude, let long = longitude else {
            return []
        }
        
        return [Double(lat)!, Double(long)!]
    }
}
