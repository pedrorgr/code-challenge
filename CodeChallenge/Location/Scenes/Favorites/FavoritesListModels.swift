//
//  FavoritesListModels.swift
//  CodeChallenge
//
//  Created by Pedro Ribeiro on 01/10/2019.
//  Copyright (c) 2019 Pedro Ribeiro. All rights reserved.

import UIKit

enum FavoritesList {
    
    enum FetchFavorites {
        
        struct Request {
        }
        
        struct Response {
            var favorites: [Favorite]
        }
        
        struct ViewModel {
            struct DisplayedFavorite {
                var name: String
                var coordinates: [Double]
            }
            var displayedFavorites: [DisplayedFavorite]
        }
    }
}
