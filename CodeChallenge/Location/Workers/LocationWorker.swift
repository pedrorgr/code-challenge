//
//  LocationWorker.swift
//  CodeChallenge
//
//  Created by Pedro Ribeiro on 26/09/2019.
//  Copyright (c) 2019 Pedro Ribeiro. All rights reserved.
//

import CoreLocation

protocol LocationStoreProtocol {
    func performSearch(query: String, latitude: String, longitude: String, completion: @escaping ([Item], LocationStoreError?) -> Void)
    func fetchLocationDetails(url: String, completion: @escaping (Location?, LocationStoreError?) -> Void)
}

enum LocationStoreError: Error {
    case LocationRequestError(String)
    case LocationParseError(String)
}

class LocationWorker {
    
    var locationStore: LocationStoreProtocol

    init(locationStore: LocationStoreProtocol) {
        self.locationStore = locationStore
    }
    
    func performSearch(query: String, latitude: String, longitude: String, completion: @escaping ([Item], LocationStoreError?) -> Void) {
    
        locationStore.performSearch(query: query, latitude: latitude, longitude: longitude) { (items, error)  in
            DispatchQueue.main.async {
                completion(items, error)
            }
        }
    }
    
    func fetchLocationDetails(url: String, completion: @escaping (Location?, LocationStoreError?) -> Void) {
        locationStore.fetchLocationDetails(url: url) { (item, error) in
            DispatchQueue.main.async {
                completion(item, error)
            }
        }
    }
}
