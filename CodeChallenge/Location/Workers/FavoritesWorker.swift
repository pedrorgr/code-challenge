//
//  FavoritesWorker.swift
//  CodeChallenge
//
//  Created by Pedro Ribeiro on 29/09/2019.
//  Copyright © 2019 Pedro Ribeiro. All rights reserved.
//

import Foundation

protocol FavoritesStoreProtocol {
    func fetchFavorites(completion: @escaping([Favorite]) -> Void)
    func fetchFavorite(id: String, completion: @escaping(Favorite?) -> Void)
    func saveLocation(location: Location, completion: @escaping(Bool) -> Void)
    func removeLocation(locationId: String, completion: @escaping(Bool) -> Void)
}

class FavoritesWorker {
    
    var favoritesStore: FavoritesStoreProtocol
    
    init(favoritesStore: FavoritesStoreProtocol) {
        self.favoritesStore = favoritesStore
    }
    
    func fetchFavorites(completion: @escaping([     Favorite]) -> Void) {
        favoritesStore.fetchFavorites { (favorites) in
            DispatchQueue.main.async {
                completion(favorites)
            }
        }
    }
    
    func fetchFavorite(id: String, completion: @escaping(Favorite?) -> Void) {
        favoritesStore.fetchFavorite(id: id) { (favorite) in
            DispatchQueue.main.async {
                completion(favorite)
            }
        }
    }
    
    func saveLocation(location: Location, completion: @escaping(Bool) -> Void) {
        favoritesStore.saveLocation(location: location) { (saved) in
            DispatchQueue.main.async {
                completion(saved)
            }
        }
    }
    
    func removeLocation(locationId: String, completion: @escaping(Bool) -> Void) {
        favoritesStore.removeLocation(locationId: locationId) { (removed) in
            DispatchQueue.main.async {
                completion(removed)
            }
        }
    }
}
