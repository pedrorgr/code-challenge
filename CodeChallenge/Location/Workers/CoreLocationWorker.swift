//
//  CoreLocationWorker.swift
//  CodeChallenge
//
//  Created by Pedro Ribeiro on 28/09/2019.
//  Copyright © 2019 Pedro Ribeiro. All rights reserved.
//

import Foundation

protocol CoreLocationStoreProtocol {
    func fetchCurrentLocation(completion: @escaping([Double]) -> Void)
}

enum CoreLocationStoreError: Error {
    case CoreLocationRequestError(String)
    case CoreLocationParseError(String)
}

class CoreLocationWorker {
    
    var coreLocationStore: CoreLocationStoreProtocol
    
    init(coreLocationStore: CoreLocationStoreProtocol) {
        self.coreLocationStore = coreLocationStore
    }
    
    func fetchCurrentLocation(completion: @escaping([Double]) -> Void) {
        coreLocationStore.fetchCurrentLocation { (coordinates) in
            DispatchQueue.main.async {
                completion(coordinates)
            }
        }
    }
}
