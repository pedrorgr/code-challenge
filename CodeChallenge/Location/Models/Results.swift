//
//  Results.swift
//  CodeChallenge
//
//  Created by Pedro Ribeiro on 28/09/2019.
//  Copyright © 2019 Pedro Ribeiro. All rights reserved.
//

import Foundation

struct SearchResults: Decodable {
    var items: [Item]
    
    enum CodingKeys: CodingKey {
        case results
    }
    
    enum ItemsKeys: CodingKey {
        case items
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let resultsContainer = try container.nestedContainer(keyedBy: ItemsKeys.self, forKey: .results)
        items = try resultsContainer.decode([Item].self, forKey: .items)
    }
}
