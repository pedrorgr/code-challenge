//
//  Item.swift
//  CodeChallenge
//
//  Created by Pedro Ribeiro on 28/09/2019.
//  Copyright © 2019 Pedro Ribeiro. All rights reserved.
//

import Foundation

struct Item: Codable, Equatable {
    var id: String
    var title: String
    var distance: Int
    var position: [Double]
    var href: String
    
    enum CodingKeys: String, CodingKey {
        case id
        case title
        case distance
        case position
        case href
    }
}
