//
//  Favorite.swift
//  CodeChallenge
//
//  Created by Pedro Ribeiro on 29/09/2019.
//  Copyright © 2019 Pedro Ribeiro. All rights reserved.
//

import Foundation
import RealmSwift

class Favorite: Object {
    @objc dynamic var id: String = ""
    @objc dynamic var name: String?
    @objc dynamic var latitude: String?
    @objc dynamic var longitude: String?
}
