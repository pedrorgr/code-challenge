//
//  Location.swift
//  CodeChallenge
//
//  Created by Pedro Ribeiro on 28/09/2019.
//  Copyright © 2019 Pedro Ribeiro. All rights reserved.
//

import Foundation

struct Location: Decodable {
    var id: String
    var name: String
    var position: [Double]
    var streetName: String?
    var postalCode: String?
    
    enum CodingKeys: String, CodingKey {
        case location
        case name
        case id = "placeId"
    }
    
    enum LocationKeys: String, CodingKey {
        case address
        case position
    }
    enum AddressKeys: String, CodingKey {
        case street
        case postalCode
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(String.self, forKey: .id)
        name = try container.decode(String.self, forKey: .name)
        
        let locationContainer = try container.nestedContainer(keyedBy: LocationKeys.self, forKey: .location)
        position = try locationContainer.decode([Double].self, forKey: .position)
        
        let addressContainer = try locationContainer.nestedContainer(keyedBy: AddressKeys.self, forKey: .address)
        do {
            streetName = try addressContainer.decode(String.self, forKey: .street)
        } catch {
            
        }
        do {
            postalCode = try addressContainer.decode(String.self, forKey: .postalCode)
        } catch {
            
        }
    }
}
