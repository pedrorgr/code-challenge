//
//  LocationAPI.swift
//  CodeChallenge
//
//  Created by Pedro Ribeiro on 28/09/2019.
//  Copyright © 2019 Pedro Ribeiro. All rights reserved.
//

import Foundation

class LocationAPI: LocationStoreProtocol {
    
    let method = "/places/v1/discover/search"
    
    func performSearch(query: String, latitude: String, longitude: String, completion: @escaping ([Item], LocationStoreError?) -> Void) {
        
        Repository.shared.get(url: method, query: query, location: "\(longitude),\(latitude)", success: { (json) in
            
            guard let json = json else {
                return
            }
            do {
                let data = try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                let searchResults = try JSONDecoder().decode(SearchResults.self, from: data)
                
                completion(searchResults.items, nil)
            } catch {
                completion([], LocationStoreError.LocationParseError(error.localizedDescription))
            }
            
        }) { (error) in
            completion([], LocationStoreError.LocationRequestError(error.localizedDescription))
        }
    }
    
    func fetchLocationDetails(url: String, completion: @escaping (Location?, LocationStoreError?) -> Void) {
        Repository.shared.get(url: url, parameters: nil, success: { (json) in
            
            guard let json = json else {
                return
            }
            do {
                let data = try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                let place = try JSONDecoder().decode(Location.self, from: data)
                completion(place, nil)
            } catch {
                completion(nil, LocationStoreError.LocationParseError(error.localizedDescription))
            }
        }) { (error) in
            completion(nil, LocationStoreError.LocationRequestError(error.localizedDescription))
        }
    }
}
