//
//  FavoritesAPI.swift
//  CodeChallenge
//
//  Created by Pedro Ribeiro on 29/09/2019.
//  Copyright © 2019 Pedro Ribeiro. All rights reserved.
//

import Foundation
import RealmSwift

class FavoritesAPI: FavoritesStoreProtocol {
    
    func fetchFavorites(completion: @escaping ([Favorite]) -> Void) {
        let realm = try! Realm()
        
        completion(Array(realm.objects(Favorite.self)))
    }
    
    func fetchFavorite(id: String, completion: @escaping (Favorite?) -> Void) {
        let realm = try! Realm()
        
        let favorite = realm.objects(Favorite.self).filter("id == '\(id)'").first
        completion(favorite)
    }
    
    func saveLocation(location: Location, completion: @escaping (Bool) -> Void) {
        let realm = try! Realm()
        
        let favorite = Favorite()
        favorite.name = location.name
        favorite.id = location.id

        if let latitude = location.position.first, let longitude = location.position.last {
            favorite.latitude = latitude.toString()
            favorite.longitude = longitude.toString()
        }
        
        try! realm.write {
            realm.add(favorite)
            completion(true)
        }
    }
    
    func removeLocation(locationId: String, completion: @escaping (Bool) -> Void) {
        let realm = try! Realm()
        
        try! realm.write {
            realm.delete(realm.objects(Favorite.self).filter("id == '\(locationId)'").first!)
            completion(true)
        }
    }
    
}
