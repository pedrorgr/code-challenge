//
//  LocationAPIMock.swift
//  CodeChallenge
//
//  Created by Pedro Ribeiro on 30/09/2019.
//  Copyright © 2019 Pedro Ribeiro. All rights reserved.
//

import Foundation

class LocationAPIMock: LocationStoreProtocol {
    func performSearch(query: String, latitude: String, longitude: String, completion: @escaping ([Item], LocationStoreError?) -> Void) {
        
    }
    
    func fetchLocationDetails(url: String, completion: @escaping (Location?, LocationStoreError?) -> Void) {
        let locationData = JSONReader.read(fileName: "Location")
        do {
            let location = try JSONDecoder().decode(Location.self, from: locationData!)
            completion(location, nil)
        } catch {
            completion(nil, LocationStoreError.LocationParseError(""))
        }
    }
}
