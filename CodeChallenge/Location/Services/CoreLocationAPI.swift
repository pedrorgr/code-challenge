//
//  LocationAPI.swift
//  CodeChallenge
//
//  Created by Pedro Ribeiro on 28/09/2019.
//  Copyright © 2019 Pedro Ribeiro. All rights reserved.
//

import Foundation
import CoreLocation

class CoreLocationAPI: NSObject, CoreLocationStoreProtocol {
    
    fileprivate let locationManager = CLLocationManager()
    fileprivate var didUpdateLocation: (([Double]) -> Void)?
    
    func fetchCurrentLocation(completion: @escaping([Double]) -> Void) {

        locationManager.requestWhenInUseAuthorization()

        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }

        didUpdateLocation = completion
    }
}

extension CoreLocationAPI: CLLocationManagerDelegate {

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }

        didUpdateLocation?([locValue.longitude, locValue.latitude])
    }

}
