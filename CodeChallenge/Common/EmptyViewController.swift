//
//  EmptyViewController.swift
//  CodeChallenge
//
//  Created by Pedro Ribeiro on 01/10/2019.
//  Copyright © 2019 Pedro Ribeiro. All rights reserved.
//

import UIKit

class EmptyViewController: UIViewController {
    
    private lazy var infoLabel = UILabel()
    
    convenience init(text: String) {
        self.init()
        self.infoLabel.text = text
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.isUserInteractionEnabled = false

        infoLabel.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(infoLabel)

        NSLayoutConstraint.activate([
            infoLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            infoLabel.centerYAnchor.constraint(equalTo: view.centerYAnchor)
        ])
    }

}
