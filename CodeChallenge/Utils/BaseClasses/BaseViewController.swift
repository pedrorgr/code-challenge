//
//  BaseViewController.swift
//  CodeChallenge
//
//  Created by Pedro Ribeiro on 01/10/2019.
//  Copyright © 2019 Pedro Ribeiro. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    
    init(nibName: String) {
        super.init(nibName: nibName, bundle: nil)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup() {
        
    }
}
