//
//  Repository.swift
//  CodeChallenge
//
//  Created by Pedro Ribeiro on 26/09/2019.
//  Copyright © 2019 Pedro Ribeiro. All rights reserved.
//

import Foundation
import Alamofire

enum RepositoryError: Equatable, Error {
    case CannotFetch
    case Unauthorized
}

class Repository {
    
    static let shared: Repository = Repository()
    
    fileprivate let baseUrl: String = Bundle.main.infoDictionary?["BaseUrl"]! as! String
    
    let headerAcceptKey = "Accept"
    let headerAcceptValue = "application/json"
    
    let appIdKey = "app_id"
    let appCodeKey = "app_code"
    let locationKey = "at"
    let queryKey = "q"
    
    func get(url: String,
             query: String,
             location: String,
             success: ((Any?) -> Void)? = nil,
             failure:((_ error: Error) -> Void)? = nil) {
        
        let parameters = [
            appIdKey: Configurations.appID,
            appCodeKey: Configurations.appCode,
            locationKey: location,
            queryKey: query,
        ]
        
        let requestUrl = baseUrl + url
        
        get(url: requestUrl, parameters: parameters, success: success, failure: failure)
    }
    
    func get(url: String,
             parameters: [String: String]?,
             success: ((Any?) -> Void)? = nil,
             failure: ((_ error: Error) -> Void)? = nil) {
        
        Alamofire.request(url, method: .get, parameters: parameters).validate().responseJSON { response in
            switch response.result {
            case .success:
                if let data = response.result.value {
                    if let success = success {
                        success(data)
                    }
                }
            case .failure(_):
                if let failure = failure {
                    failure(RepositoryError.CannotFetch)
                }
            }
        }
    }
}
