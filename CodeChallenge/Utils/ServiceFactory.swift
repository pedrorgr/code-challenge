//
//  ServiceFactory.swift
//  CodeChallenge
//
//  Created by Pedro Ribeiro on 28/09/2019.
//  Copyright © 2019 Pedro Ribeiro. All rights reserved.
//

import Foundation

enum ServiceFactoryType {
    case api
    case mock
}

struct ServiceFactory {
    
    static var shared: ServiceFactory = ServiceFactory()

    var type: ServiceFactoryType = .api

    func configure(type: ServiceFactoryType) {
        ServiceFactory.shared.type = type
    }
    
    func locationService() -> LocationStoreProtocol {
        switch type {
        case .api:
            return LocationAPI()
        case .mock:
            return LocationAPIMock()
        }
    }
    
    func favoritesService() -> FavoritesStoreProtocol {
        FavoritesAPI()
    }
    
    func coreLocationService() -> CoreLocationStoreProtocol {
        return CoreLocationAPI()
    }
}
