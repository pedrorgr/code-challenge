//
//  CodeChallenge+Double.swift
//  CodeChallenge
//
//  Created by Pedro Ribeiro on 29/09/2019.
//  Copyright © 2019 Pedro Ribeiro. All rights reserved.
//

import Foundation

extension Double {
    
    func toString() -> String {
        return String(format: "%.2f", self)
    }
}
