//
//  CodeChallenge+UIViewController.swift
//  CodeChallenge
//
//  Created by Pedro Ribeiro on 28/09/2019.
//  Copyright © 2019 Pedro Ribeiro. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func alert(title: String, message: String, okActionHandler: (() -> Void)? = nil, cancelActionHandler: (() -> Void)? = nil) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "Ok", style: .default) { (_) in
            if okActionHandler != nil {
                okActionHandler!()
            }
        }
        alertController.addAction(OKAction)
        
        if cancelActionHandler != nil {
            let cancelAction = UIAlertAction(title: "Cancel", style: .default) { (_) in
                cancelActionHandler!()
            }
            alertController.addAction(cancelAction)
        }
        
        present(alertController, animated: true, completion: nil)
    }
    
    func add(_ child: UIViewController) {
        addChild(child)
        view.addSubview(child.view)
        child.didMove(toParent: self)
    }
    
    func remove() {

        guard parent != nil else {
            return
        }
        
        willMove(toParent: nil)
        view.removeFromSuperview()
        removeFromParent()
    }
}
